#!/bin/env python
# This cleans existing data

import os

# Reset GSettings
for schema in ['Dialogs.Add', 'Dialogs.Extract', 'Dialogs.LastOutput', 'Dialogs.New',
                'FileSelector', 'General', 'Listing', 'UI']:
    os.system("gsettings reset-recursively org.gnome.FileRoller.%s" % schema)
