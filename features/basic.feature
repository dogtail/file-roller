Feature: Basic tests

  @new_tar_from_menu
  Scenario: New tar archive from the menu
    * Start file-roller
    * Create new ".tar" type archive called "tararchive" in testing location
    * Add "testfile1.test,testfile3.test,testfile5.test" files into open archive
    * Click "Quit" in GApplication menu
    Then Check "tararchive.tar" contains "testfile1.test,testfile3.test,testfile5.test"

  @rhbz1044633
  @new_zip_from_menu
  Scenario: New zip archive from the menu
    * Start file-roller
    * Create new ".zip" type archive called "ziparchive" in testing location
    * Add "testfile2.test,testfile3.test,testfile4.test" files into open archive
    * Click "Quit" in GApplication menu
    Then Check "ziparchive.zip" contains "testfile2.test,testfile3.test,testfile4.test"


  @new_bz2_from_menu
  Scenario: New tar.bz2 archive from the menu
    * Start file-roller
    * Create new ".tar.bz2" type archive called "bz2archive" in testing location
    * Add "testfile2.test,testfile4.test,testfile5.test" files into open archive
    * Click "Quit" in GApplication menu
    Then Check "bz2archive.tar.bz2" contains "testfile2.test,testfile4.test,testfile5.test"


  @new_xz_from_menu
  Scenario: New tar.xz archive from the menu
    * Start file-roller
    * Create new ".tar.xz" type archive called "xzarchive" in testing location
    * Add "testfile2.test,testfile4.test,testfile1.test" files into open archive
    * Click "Quit" in GApplication menu
    Then Check "xzarchive.tar.xz" contains "testfile2.test,testfile4.test,testfile1.test"


  @new_war_from_menu
  Scenario: New war archive from the menu
    * Start file-roller
    * Create new ".war" type archive called "archive" in testing location
    * Add "testfile1.test,testfile3.test,testfile5.test" files into open archive
    * Click "Quit" in GApplication menu
    Then Check "archive.war" contains "testfile1.test,testfile3.test,testfile5.test"


  @new_ar_from_menu
  Scenario: New ar archive from the menu
    * Start file-roller
    * Create new ".ar" type archive called "archive" in testing location
    * Add "testfile1.test,testfile3.test,testfile5.test" files into open archive
    * Click "Quit" in GApplication menu
    Then Check "archive.ar" contains "testfile1.test,testfile3.test,testfile5.test"


  @new_iso_from_menu
  Scenario: New iso archive from the menu
    * Start file-roller
    * Create new ".iso" type archive called "archive" in testing location
    * Add "testfile1.test,testfile3.test,testfile5.test" files into open archive
    * Click "Quit" in GApplication menu
    Then Check "archive.iso" contains "testfile1.test,testfile3.test,testfile5.test"


  @new_jar_from_menu
  Scenario: New jar archive from the menu
    * Start file-roller
    * Create new ".jar" type archive called "archive" in testing location
    * Add "testfile1.test,testfile3.test,testfile5.test" files into open archive
    * Click "Quit" in GApplication menu
    Then Check "archive.jar" contains "testfile1.test,testfile3.test,testfile5.test"


  @new_tar_lzo_from_menu
  Scenario: New tar.lz archive from the menu
    * Start file-roller
    * Create new ".tar.lzo" type archive called "archive" in testing location
    * Add "testfile1.test,testfile3.test,testfile5.test" files into open archive
    * Click "Quit" in GApplication menu
    Then Check "archive.tar.lzo" contains "testfile1.test,testfile3.test,testfile5.test"


  @new_tar_gz_from_menu
  Scenario: New tar.gz archive from the menu
    * Start file-roller
    * Create new ".tar.gz" type archive called "archive" in testing location
    * Add "testfile1.test,testfile3.test,testfile5.test" files into open archive
    * Click "Quit" in GApplication menu
    Then Check "archive.tar.gz" contains "testfile1.test,testfile3.test,testfile5.test"


  @open_existing_tar
  Scenario: Open an existing .tar file
    * Make "tarsample.tar" from "testfile1.test,testfile3.test,testfile5.test"
    * Start file-roller
    * Open "tarsample.tar" archive
    Then See "testfile1.test,testfile3.test,testfile5.test" files are present


  @open_existing_zip
  Scenario: Open an existing .zip file
    * Make "zipsample.zip" from "testfile2.test,testfile3.test,testfile4.test"
    * Start file-roller
    * Open "zipsample.zip" archive
    Then See "testfile2.test,testfile3.test,testfile4.test" files are present


  @open_existing_tar_gz
  Scenario: Open an existing .tar.gz file
    * Make "sample.tar.gz" from "testfile2.test,testfile3.test,testfile4.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    Then See "testfile2.test,testfile3.test,testfile4.test" files are present


  @open_existing_bz2
  Scenario: Open an existing .tar.bz2 file
    * Make "bz2sample.tar.bz2" from "testfile2.test,testfile4.test,testfile5.test"
    * Start file-roller
    * Open "bz2sample.tar.bz2" archive
    Then See "testfile2.test,testfile4.test,testfile5.test" files are present


  @open_existing_xz
  Scenario: Open an existing .tar.xz file
    * Make "xzsample.tar.xz" from "testfile2.test,testfile4.test,testfile1.test"
    * Start file-roller
    * Open "xzsample.tar.xz" archive
    Then See "testfile2.test,testfile4.test,testfile1.test" files are present


  @open_nonarchive
  Scenario: Open an existing .gz file
    * Make "gzsample.gz" from "testfile2.test"
    * Start file-roller
    * Open "gzsample.gz" archive
    Then See "testfile2.test" files are present


  @open_existing_war
  Scenario: Open an existing .war file
    * Make "warsample.war" from "testfile2.test,testfile3.test,testfile4.test"
    * Start file-roller
    * Open "warsample.war" archive
    Then See "testfile2.test,testfile3.test,testfile4.test" files are present


  @open_existing_jar
  Scenario: Open an existing .jar file
    * Make "jarsample.jar" from "testfile2.test,testfile3.test,testfile4.test"
    * Start file-roller
    * Open "jarsample.jar" archive
    Then See "testfile2.test,testfile3.test,testfile4.test" files are present


  @open_existing_tar_lzo
  Scenario: Open an existing .tar.lzo file
    * Make "sample.tar.lzo" from "testfile2.test,testfile3.test,testfile4.test"
    * Start file-roller
    * Open "sample.tar.lzo" archive
    Then See "testfile2.test,testfile3.test,testfile4.test" files are present
