# -*- coding: UTF-8 -*-

import os
import os.path
import sys
import traceback
from common_steps import App, common_before_all, common_after_step, common_after_scenario
from dogtail.rawinput import keyCombo
from dogtail.tree import root
from dogtail.config import config
from getpass import getuser
from time import sleep


def before_all(context):
    """Setup file-roller stuff
    Being executed before all features
    """
    try:
        if getuser() != 'test':
            print('\nThese tests have to be run under user "test" with "redhat" password.')
            sys.exit(-1)

        common_before_all(context)

        # Do the cleanup
        os.system("python3 cleanup.py > /dev/null")

        # Kill initial setup
        os.system("killall /usr/libexec/gnome-initial-setup")

        context.app = App('file-roller', recordVideo=True, desktopFileName='org.gnome.FileRoller')

        config.typingDelay = 0.5

    except Exception as e:
        print("Error in before_all: %s" % e.message)
        traceback.print_exc(file=sys.stdout)


def after_step(context, step):
    sleep(0.5) # default afterstep
    try:
        common_after_step(context, step)

    except Exception as e:
        print("Error in after_step: %s" % e.message)
        traceback.print_exc(file=sys.stdout)


def before_scenario(context, scenario):
    try:
        if 'nautilus' in scenario.tags:
        # close nautilus window if open
            try:
                home = root.application('nautilus').child('Home', roleName="toggle button", retry=False)
                home.grabFocus()
                keyCombo('<Control>Q')
            except:
                pass

        user = getuser()
        context.curdir = os.getcwd()
        os.system('rm -rf /home/%s/Music/*' % user)
        os.system('echo testfile1 > /home/%s/Music/testfile1.test' % user)
        os.system('echo testfile2 > /home/%s/Music/testfile2.test' % user)
        os.system('echo testfile3 > /home/%s/Music/testfile3.test' % user)
        os.system('echo testfile4 > /home/%s/Music/testfile4.test' % user)
        os.system('echo testfile5 > /home/%s/Music/testfile5.test' % user)
        os.system('mkdir /home/%s/Music/testdir' % user)
        os.system('echo testfile6 > /home/%s/Music/testdir/testfile6.test' % user)
        os.system('echo testfile7 > /home/%s/Music/testdir/testfile7.test' % user)
        os.system('echo testfile8 > /home/%s/Music/testdir/testfile8.test' % user)
        os.system('echo testfile9 > /home/%s/Music/testdir/testfile9.test' % user)
        os.system('echo testfile10 > /home/%s/Music/testdir/testfile10.test' % user)
        os.system('echo testfile11 > /home/%s/Music/testdir/testfile11.test' % user)
        os.system('echo testfile111 > /home/%s/Music/testdir/testfile111.test' % user)

    except Exception as e:
        print("Error in before_scenario: %s" % e.message)
        traceback.print_exc(file=sys.stdout)


def after_scenario(context, scenario):
    try:
        if 'extract_from_command_line_specify_dir' in scenario.tags:
            os.system('rm -f /tmp/testfile1.test')
        if 'show_help' in scenario.tags:
            os.system('killall yelp')

        if 'nautilus' in scenario.tags:
        # close nautilus window if open
            try:
                home = root.application('nautilus').child('Home', roleName="toggle button", retry=False)
                home.grabFocus()
                keyCombo('<Control>Q')
            except:
                pass

        common_after_scenario(context, scenario)

        if 'debug' not in scenario.tags:
            # leave the artifacts in for debuggig (test tag present)
            os.system("rm -rf /home/%s/Music/*" % getuser())

        # Do the cleanup
        os.system("python cleanup.py")

    except Exception as e:
        print("Error in after_scenario: %s" % e.message)
        traceback.print_exc(file=sys.stdout)
