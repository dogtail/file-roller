Feature: Extraction tests


  @extract_zip
  Scenario: Extract zip archive
    * Make "sample.zip" from "testdir,testfile1.test,testfile3.test,testfile5.test"
    * Start file-roller
    * Open "sample.zip" archive
    * Click the toolbar "Extract" button
    * Extract files into the testing directory
    Then Check files "testdir,testfile6.test,testfile1.test,testfile3.test,testfile5.test" have been extracted


  @extract_tar
  Scenario: Extract tar archive
    * Make "sample.tar" from "testdir,testfile1.test,testfile3.test,testfile5.test"
    * Start file-roller
    * Open "sample.tar" archive
    * Click the toolbar "Extract" button
    * Extract files into the testing directory
    Then Check files "testdir,testfile6.test,testfile1.test,testfile3.test,testfile5.test" have been extracted


  @extract_jar
  Scenario: Extract jar archive
    * Make "sample.jar" from "testdir,testfile1.test,testfile3.test,testfile5.test"
    * Start file-roller
    * Open "sample.jar" archive
    * Click the toolbar "Extract" button
    * Extract files into the testing directory
    Then Check files "testdir,testfile6.test,testfile1.test,testfile3.test,testfile5.test" have been extracted


  @rhbz950515
  @extract_tar_lzo
  Scenario: Extract tar.lzo archive
    * Make "sample.tar.lzo" from "testdir,testfile1.test,testfile3.test,testfile5.test"
    * Start file-roller
    * Open "sample.tar.lzo" archive
    * Click the toolbar "Extract" button
    * Extract files into the testing directory
    Then Check files "testdir,testfile6.test,testfile1.test,testfile3.test,testfile5.test" have been extracted


  @extract_tar_xz
  Scenario: Extract tar.xz archive
    * Make "sample.tar.xz" from "testdir,testfile1.test,testfile3.test,testfile5.test"
    * Start file-roller
    * Open "sample.tar.xz" archive
    * Click the toolbar "Extract" button
    * Extract files into the testing directory
    Then Check files "testdir,testfile6.test,testfile1.test,testfile3.test,testfile5.test" have been extracted


  @extract_tar_bz2
  Scenario: Extract tar.bz2 archive
    * Make "sample.tar.bz2" from "testdir,testfile1.test,testfile3.test,testfile5.test"
    * Start file-roller
    * Open "sample.tar.bz2" archive
    * Click the toolbar "Extract" button
    * Extract files into the testing directory
    Then Check files "testdir,testfile6.test,testfile1.test,testfile3.test,testfile5.test" have been extracted


  @extract_war
  Scenario: Extract war archive
    * Make "sample.war" from "testdir,testfile1.test,testfile3.test,testfile5.test"
    * Start file-roller
    * Open "sample.war" archive
    * Click the toolbar "Extract" button
    * Extract files into the testing directory
    Then Check files "testdir,testfile6.test,testfile1.test,testfile3.test,testfile5.test" have been extracted


  @extract_all_files_archive_button
  Scenario: Extract all files via archive button
    * Make "sample.tar.gz" from "testdir,testfile1.test,testfile3.test,testfile5.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Click the toolbar "Extract" button
    * Extract files into the testing directory
    Then Check files "testdir,testfile6.test,testfile1.test,testfile3.test,testfile5.test" have been extracted


  @extract_selected_files_archive_button
  Scenario: Extract selected files via archive button
    * Make "sample.tar.gz" from "testfile1.test,testfile3.test,testfile5.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Select files "testfile1.test,testfile5.test"
    * Click the toolbar "Extract" button
    * Extract files into the testing directory
    Then Check files "testfile1.test,testfile5.test" have been extracted


  @extract_directory_archive_button
  Scenario: Extract selected directory via archive button
    * Make "sample.tar.gz" from "testdir,testfile2.test,testfile4.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Select files "testdir"
    * Click the toolbar "Extract" button
    * Extract files into the testing directory
    Then Check files "testdir,testfile6.test,testfile7.test,testfile8.test" have been extracted


  @extract_files_using_filter
  Scenario: Extract files using a filter
    * Make "sample.tar.gz" from "testdir,testfile1.test,testfile2.test,testfile4.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Click the toolbar "Extract" button
    * Set file filter to "testfile1*" and extract into the testing directory
    Then Check files "testdir,testfile1.test,testfile10.test,testfile111.test" have been extracted


  @extract_files_without_dir_structure
  Scenario: Extract files without directory structure
    * Make "sample.tar.gz" from "testdir,testfile1.test,testfile2.test,testfile4.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Click the toolbar "Extract" button
    * Toggle off the "Keep directory structure" check box
    * Extract files into the testing directory
    Then Check files "testfile1.test,testfile2.test,testfile10.test,testfile111.test" have been extracted
    Then Check files "testdir" have not been extracted


  @extract_files_replacing
  Scenario: Extract files replacing original content
    * Make "sample.zip" from "testdir,testfile1.test,testfile3.test,testfile5.test"
    * Start file-roller
    * Open "sample.zip" archive
    * Click the toolbar "Extract" button
    * Extract files into the testing directory
    * Click "Close" to confirm the dialog
    * Click the toolbar "Extract" button
    * Extract files into the current directory
    * Click "Replace All" to confirm the dialog
    Then Check files "testdir,testfile6.test,testfile1.test,testfile3.test,testfile5.test" have been extracted


  @extract_files_keeping_newer
  Scenario: Extract files keeping newer files on replace
    * Make "sample.zip" from "testfile1.test"
    * Start file-roller
    * Open "sample.zip" archive
    * Click the toolbar "Extract" button
    * Extract files into the testing directory
    * Click "Close" to confirm the dialog
    * Add text "more text" in extracted file "testfile1.test"
    * Click the toolbar "Extract" button
    * Toggle on the "Do not overwrite newer files" check box
    * Extract files into the current directory
    * Click "Replace All" to confirm the dialog
    Then Check files "testfile1.test" have been extracted
    Then Check extracted file "testfile1.test" contains "more text" text


  @extract_selected_files_context
  Scenario: Extract selected files via context menu
    * Make "sample.tar.gz" from "testfile1.test,testfile2.test,testfile4.test,testfile5.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Select files "testfile2.test,testfile1.test,testfile5.test"
    * Invoke context menu over "testfile5.test" and choose "Extract…" action
    * Extract files into the testing directory
    Then Check files "testfile1.test,testfile2.test,testfile5.test" have been extracted


  @extract_selected_files_shortcut
  Scenario: Extract selected files via keyboard shortcut
    * Make "sample.tar.gz" from "testfile1.test,testfile2.test,testfile4.test,testfile5.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Select files "testfile2.test,testfile1.test,testfile5.test"
    * Invoke "<Ctrl>E" shortcut
    * Extract files into the testing directory
    Then Check files "testfile1.test,testfile2.test,testfile5.test" have been extracted


  @extract_directory_context
  Scenario: Extract selected directory via context menu
    * Make "sample.tar.gz" from "testdir,testfile2.test,testfile4.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Select files "testdir"
    * Invoke context menu over "testdir" and choose "Extract…" action
    * Extract files into the testing directory
    Then Check files "testdir,testfile6.test,testfile7.test,testfile8.test" have been extracted
