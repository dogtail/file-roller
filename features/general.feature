Feature: General tests


  @rhbz1222955
  @readd_preselected_files
  Scenario: Re-add preselected files
    * Start file-roller
    * Create new ".zip" type archive called "sample" in testing location
    * Add "testfile1.test,testfile2.test,testfile3.test" files into open archive
    * See "testfile1.test,testfile2.test,testfile3.test" files are present
    * Spawn context menu and delete "All files"
    * See "testfile1.test,testfile2.test,testfile3.test" files are not present
    * Click the toolbar "Add" button
    # issue point - files are preselected but Add button is greyed out
    * Click "Add" to confirm the dialog
    Then Check "sample.zip" contains "testfile1.test,testfile2.test,testfile3.test"


  @rhbz1186481
  @extract_from_command_line
  Scenario: Extract archive from command line
    * Make "sample.tar.gz" from "testfile1.test,testfile2.test,testfile3.test,testfile4.test,testfile5.test"
    * Start file-roller with "--default-dir=/tmp --extract sample.tar.gz" parameters from the sample archive directory
    * Extract files into the testing directory
    # if the dialog doesn't showed up roller has crashed silently already
    Then Terminal instance exited with return code "0"
    Then Check files "testfile1.test,testfile2.test,testfile3.test,testfile4.test,testfile5.test" have been extracted


  @rhbz1186481
  @extract_from_command_line_specify_dir
  Scenario: Extract archive from command line specify directory
    * Make "sample.tar.gz" from "testfile1.test"
    * Start file-roller with "--extract sample.tar.gz --extract-to=/tmp" parameters from the sample archive directory
    Then Terminal instance exited with return code "0"
    Then File "/tmp/testfile1.test" exists


  @add_to_command_line
  Scenario: Add file in archive via --add-to command line option
    * Make "sample.tar.gz" from "testfile1.test"
    * Start file-roller with "--add-to=sample.tar.gz testdir testfile1.test" parameters from the sample archive directory
    Then Terminal instance exited with return code "0"
    Then Check "sample.tar.gz" contains "testdir/testfile111.test,testfile1.test"


  @add_command_line_fuction
  Scenario: Create archive from command line via --add option
    * Start file-roller with "--add testdir testfile1.test" parameters from the sample archive directory
    * Type "sample"
    * Select type ".zip"
    * Click "Create" to confirm the dialog
    Then Terminal instance exited with return code "0"
    Then Check "sample.zip" contains "testdir/testfile6.test,testfile1.test"


  @add_directory_in_existing_archive
  Scenario: Add folder in existing archive
    * Make "sample.tar.gz" from "testfile1.test,testfile2.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Add "testdir" files into open archive
    Then Check "sample.tar.gz" contains "testdir,testfile7.test,testfile1.test,testfile2.test"


  @add_files_in_existing_archive
  Scenario: Add files in existing archive
    * Make "sample.tar.gz" from "testfile1.test,testfile2.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Add "testfile4.test,testfile5.test" files into open archive
    Then Check "sample.tar.gz" contains "testfile1.test,testfile2.test,testfile4.test,testfile5.test"


  @rhbz1222938
  @add_files_by_filter
  Scenario: Add files by filter
    * Start file-roller
    * Create new ".tar.gz" type archive called "archive" in testing location
    * Click the toolbar "Add" button
    * Navigate to the testing directory in the dialog
    * Set "Include files" filter to "testfile1*" text
    * Check-in files "testfile1.test,testfile2.test,testfile3.test"
    * Click "Add" to confirm the dialog
    Then See "testfile1.test" files are present
    Then See "testfile2.test,testfile3.test" files are not present


  @copy_paste_file_under_same_location
  Scenario: Copy and paste file into the same location
    * Make "sample.tar.gz" from "testfile1.test,testfile2.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Invoke context menu over "testfile1.test" and choose "Copy" action
    * Invoke context menu over "testfile2.test" and choose "Paste" action
    * Type "/" and confirm
    Then Check "sample.tar.gz" contains "testfile1.test,testfile2.test"


  @copy_paste_file_new_location
  Scenario: Copy and paste file into a new location
    * Make "sample.tar.gz" from "testfile1.test,testfile2.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Invoke context menu over "testfile1.test" and choose "Copy" action
    * Invoke context menu over "testfile2.test" and choose "Paste" action
    * Type "/newdir" and confirm
    Then Check "sample.tar.gz" contains "newdir/testfile1.test,testfile1.test,testfile2.test"


  @copy_paste_dir_new_location
  Scenario: Copy and paste directory into new location
    * Make "sample.tar.gz" from "testdir,testfile1.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Invoke context menu over "testdir" and choose "Copy" action
    * Invoke context menu over "testfile1.test" and choose "Paste" action
    * Type "/newtestdir" and confirm
    Then Check "sample.tar.gz" contains "newtestdir/testdir,testfile1.test,testfile7.test"


  @cut_paste_file
  Scenario: Cut and paste a file
    * Make "sample.tar.gz" from "testfile1.test,testfile2.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Invoke context menu over "testfile1.test" and choose "Cut" action
    * Invoke context menu over "testfile2.test" and choose "Paste" action
    * Type "/newlocation" and confirm
    Then See "newlocation,testfile2.test" files are present
    Then See "testfile1.test" files are not present
    Then Check "sample.tar.gz" contains "newlocation/testfile1.test,testfile2.test"
    Then Check "sample.tar.gz" does not contain " testfile1.test"


  @rhbz1243347
  @cut_paste_file_same_location
  Scenario: Cut and paste a file
    * Make "sample.tar.gz" from "testfile1.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Invoke context menu over "testfile1.test" and choose "Cut" action
    * Invoke context menu over "testfile1.test" and choose "Paste" action
    * Click "Paste" to confirm the dialog
    Then See "testfile1.test" files are present
    Then Check "sample.tar.gz" contains "testfile1.test"


  @delete_selected_files
  Scenario: Delete selected files
    * Make "sample.tar.gz" from "testfile1.test,testfile2.test,testfile3.test,testfile4.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Select files "testfile1.test,testfile3.test"
    * Spawn context menu and delete "Selected files"
    Then See "testfile2.test,testfile4.test" files are present
    Then See "testfile1.test,testfile3.test" files are not present
    Then Check "sample.tar.gz" contains "testfile2.test,testfile4.test"
    Then Check "sample.tar.gz" does not contain "testfile1.test,testfile3.test"


  @delete_file_key
  Scenario: Delete selected files with delete key
    * Make "sample.tar.gz" from "testfile1.test,testfile2.test,testfile3.test,testfile4.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Select files "testfile1.test,testfile3.test"
    * Press key "del"
    * Click "Delete" to confirm the dialog
    Then See "testfile2.test,testfile4.test" files are present
    Then See "testfile1.test,testfile3.test" files are not present
    Then Check "sample.tar.gz" contains "testfile2.test,testfile4.test"
    Then Check "sample.tar.gz" does not contain "testfile1.test,testfile3.test"


  @rhbz1228645
  @delete_all_files_and_dirs_tar
  Scenario: Delete all files and directories in tar
    * Make "sample.tar.gz" from "testdir,testfile1.test,testfile2.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Spawn context menu and delete "All files"
    Then Check "sample.tar.gz" archive is empty
    Then See "testdir,testfile1.test,testfile2.test" files are not present


  @rhbz1228645
  @delete_all_files_and_dirs_zip
  Scenario: Delete all files and directories in zip
    * Make "sample.zip" from "testdir,testfile1.test,testfile2.test"
    * Start file-roller
    * Open "sample.zip" archive
    * Spawn context menu and delete "All files"
    Then Check "sample.zip" archive is empty
    Then See "testdir,testfile1.test,testfile2.test" files are not present


  @delete_files_filter
  Scenario: Delete files specified by filter
    * Make "sample.tar.gz" from "testdir,testfile1.test,testfile2.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Spawn context menu and delete "testfile1*"
    Then See "testfile1.test" files are not present
    Then Check "sample.tar.gz" does not contain "testfile1.test,testfile10.test,testfile11.test,testfile111.test"


  @delete_all_filter
  Scenario: Delete all files and directories by filter
    * Make "sample.tar.gz" from "testdir,testfile1.test,testfile2.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Spawn context menu and delete "*"
    Then Check "sample.tar.gz" archive is empty


  @open_with
  Scenario: Open and edit file with custom application
    * Make "sample.tar.gz" from "testfile1.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Invoke context menu over "testfile1.test" and choose "Open With…" action
    * Choose and open with "Text Editor" application
    * Type "editing text" and save file in gedit
    * Click "Update" to confirm the dialog
    * Click the toolbar "Extract" button
    * Extract files into the testing directory
    Then Check extracted file "testfile1.test" contains "editing text" text


  @open_edit_file_zip
  Scenario: Open and edit a file in zip archive
    * Make "sample.zip" from "testfile1.test"
    * Start file-roller
    * Open "sample.zip" archive
    * Invoke context menu over "testfile1.test" and choose "Open" action
    * Type "editing text" and save file in gedit
    * Click "Update" to confirm the dialog
    * Click the toolbar "Extract" button
    * Extract files into the testing directory
    Then Check extracted file "testfile1.test" contains "editing text" text


  @open_edit_file_tar_gz
  Scenario: Open and edit a file in tar.gz archive
    * Make "sample.tar.gz" from "testfile1.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Invoke context menu over "testfile1.test" and choose "Open" action
    * Type "editing text" and save file in gedit
    * Click "Update" to confirm the dialog
    * Click the toolbar "Extract" button
    * Extract files into the testing directory
    Then Check extracted file "testfile1.test" contains "editing text" text


  @open_edit_file_war
  Scenario: Open and edit a file in war archive
    * Make "sample.war" from "testfile1.test"
    * Start file-roller
    * Open "sample.war" archive
    * Invoke context menu over "testfile1.test" and choose "Open" action
    * Type "editing text" and save file in gedit
    * Click "Update" to confirm the dialog
    * Click the toolbar "Extract" button
    * Extract files into the testing directory
    Then Check extracted file "testfile1.test" contains "editing text" text


  @open_edit_file_nonarchive_gz
  Scenario: Open and edit a file in nonarchive gz archive
    * Make "sample.gz" from "testfile1.test"
    * Start file-roller
    * Open "sample.gz" archive
    * Invoke context menu over "testfile1.test" and choose "Open" action
    * Type "editing text" and save file in gedit
    * Click "Update" to confirm the dialog
    * Click the toolbar "Extract" button
    * Extract files into the testing directory
    Then Check extracted file "testfile1.test" contains "editing text" text


  @rename_file
  Scenario: Rename a file
    * Make "sample.tar.gz" from "testfile1.test,testfile2.test,testfile3.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Invoke context menu over "testfile2.test" and choose "Rename…" action
    * Type "~!@#$%^&()_.test" and confirm
    Then See "~!@#$%^&()_.test,testfile1.test,testfile3.test" files are present
    Then See "testfile2.test" files are not present
    Then Check "sample.tar.gz" contains "~!@#$%^&()_.test"


  @rename_file_shortcut
  Scenario: Rename a file using a shortcut
    * Make "sample.tar.gz" from "testfile1.test,testfile2.test,testfile3.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Select files "testfile2.test"
    * Press "F2"
    * Type "~!@#$%^&()_.test" and confirm
    Then See "~!@#$%^&()_.test,testfile1.test,testfile3.test" files are present
    Then See "testfile2.test" files are not present
    Then Check "sample.tar.gz" contains "~!@#$%^&()_.test"


  @rename_dir
  Scenario: Rename a directory
    * Make "sample.tar.gz" from "testdir,testfile1.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Invoke context menu over "testdir" and choose "Rename…" action
    * Type "renamedir" and confirm
    Then See "renamedir,testfile1.test" files are present
    Then See "testdir" files are not present
    Then Check "sample.tar.gz" contains "renamedir/testfile6.test,renamedir/testfile111.test"


  @search_files
  Scenario: Search files in archive
    * Make "sample.tar.gz" from "testdir,testfile1.test,testfile2.test,testfile3.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Click the toolbar "Search" button
    * Type "testfile1"
    Then See "testfile1.test,testfile10.test,testfile11.test,testfile111.test" files are present
    Then See "testfile6.test,testfile3.test" files are not present


  @create_protected_archive
  Scenario: Create a password protected archive
    * Start file-roller
    * Come into New Archive dialog filling filename "sample" and type ".zip"
    * Set password to "redhat" in Other Options section
    * Click "Create" to confirm the dialog
    * Add "testdir,testfile1.test,testfile2.test,testfile3.test" files into open archive
    Then Check archive "sample.zip" requires a "redhat" password and contains "testdir,testfile1.test,testfile2.test,testfile3.test"


  @encrypt_archive
  Scenario: Encrypt an archive
    * Make "sample.zip" from "testdir,testfile1.test,testfile2.test,testfile3.test"
    * Start file-roller
    * Open "sample.zip" archive
    * Choose "Password…" from the archive menu
    * Type "redhat" and confirm
    Then Check archive "sample.zip" requires a "redhat" password and contains "testdir,testfile1.test,testfile2.test,testfile3.test"


  @extract_encrypted_archive
  Scenario: Encrypt a zip archive
    * Make encrypted zip "sample.zip" containing "testfile1.test,testfile2.test,testfile3.test" files
    * Start file-roller
    * Open "sample.zip" archive
    * Click the toolbar "Extract" button
    * Extract files into the testing directory
    * Type "redhat" and confirm
    Then Check files "testfile1.test,testfile2.test,testfile3.test" have been extracted


  @encrypt_archive_war
  Scenario: Encrypt a war archive
    * Make "sample.war" from "testdir,testfile1.test,testfile2.test,testfile3.test"
    * Start file-roller
    * Open "sample.war" archive
    * Choose "Password…" from the archive menu
    * Type "redhat" and confirm
    Then Check archive "sample.war" requires a "redhat" password and contains "testdir,testfile1.test,testfile2.test,testfile3.test"


  @encrypt_archive_jar
  Scenario: Encrypt a jar archive
    * Make "sample.jar" from "testdir,testfile1.test,testfile2.test,testfile3.test"
    * Start file-roller
    * Open "sample.jar" archive
    * Choose "Password…" from the archive menu
    * Type "redhat" and confirm
    Then Check archive "sample.jar" requires a "redhat" password and contains "testdir,testfile1.test,testfile2.test,testfile3.test"


  @modify_encrypted_file
  Scenario: Modify ecrypted archive
    * Make encrypted zip "sample.zip" containing "testfile1.test,testfile2.test,testfile3.test" files
    * Start file-roller
    * Open "sample.zip" archive
    * Invoke context menu over "testfile1.test" and choose "Open" action
    * Type "redhat" and confirm
    * Type "editing text" and save file in gedit
    * Click "Update" to confirm the dialog
    * Click the toolbar "Extract" button
    * Extract files into the testing directory
    Then Check extracted file "testfile1.test" contains "editing text" text
    Then Check archive "sample.zip" requires a "redhat" password and contains "testfile1.test,testfile2.test,testfile3.test"


  @check_integrity
  Scenario: Test integrity of an archive
    * Make "sample.zip" from "testdir,testfile1.test,testfile2.test,testfile3.test"
    * Start file-roller
    * Open "sample.zip" archive
    * Choose "Test Integrity" from the archive menu
    * Look for text "No errors detected" in the report


  @check_integrity_war
  Scenario: Test integrity of an war archive
    * Make "sample.war" from "testdir,testfile1.test,testfile2.test,testfile3.test"
    * Start file-roller
    * Open "sample.war" archive
    * Choose "Test Integrity" from the archive menu
    * Look for text "No errors detected" in the report

  @rhbz1233661
  @check_integrity_empty
  Scenario: Test integrity of a new empty archive
    * Start file-roller
    * Create new ".zip" type archive called "sample" in testing location
    * Choose "Test Integrity" from the archive menu
    * Look for text "No errors detected" in the report


  @rhbz1233853
  @rename_file_encrypted
  Scenario: Rename a file in ecrypted archive
    * Make encrypted zip "sample.zip" containing "testdir,testfile1.test,testfile2.test,testfile3.test" files
    * Start file-roller
    * Open "sample.zip" archive
    * Invoke context menu over "testfile2.test" and choose "Rename…" action
    * Type "dontdeleteme.test" and confirm
    * Type "redhat" and confirm
    Then See "dontdeleteme.test,testfile1.test,testfile3.test" files are present
    Then See "testfile2.test" files are not present
    Then Check "sample.zip" contains "dontdeleteme.test"


  @properties_zip_file
  Scenario: Check properties of a zip archive
    * Make "sample.zip" from "testdir,testfile1.test,testfile2.test,testfile3.test"
    * Start file-roller
    * Open "sample.zip" archive
    * Choose "Properties" from the archive menu
    * Check the "sample.zip" properties information is valid


  @properties_tar_gz_file
  Scenario: Check properties of a tar.gz archive
    * Make "sample.tar.gz" from "testdir,testfile1.test,testfile2.test,testfile3.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Choose "Properties" from the archive menu
    * Check the "sample.tar.gz" properties information is valid


  @save_as_other_archive
  Scenario: Save archive as another archive type
    * Make "sample.tar.gz" from "testdir,testfile1.test,testfile2.test,testfile3.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Choose "Save As…" from the archive menu
    * Select type ".zip"
    * Click "Save" to confirm the dialog
    Then Check "sample.zip" contains "testdir/testfile6.test,testfile1.test,testfile3.test"
    Then Check "sample.tar.gz" contains "testdir/testfile6.test,testfile1.test,testfile3.test"


  @go_home
  Scenario: Navigate into the home location
    * Make "sample.tar.gz" from "testdir,testfile1.test,testfile2.test,testfile3.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Invoke context menu over "testdir" and choose "Open" action
    * See "testfile6.test,testfile9.test" files are present
    * See "testdir,testfile1.test,testfile2.test" files are not present
    * Click the toolbar "Home" button
    Then See "testfile6.test,testfile9.test" files are not present
    Then See "testdir,testfile1.test,testfile2.test" files are present


  @view_all_files
  Scenario: View all files
    * Make "sample.tar.gz" from "testdir,testfile1.test,testfile2.test,testfile3.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Click "View All Files" in application menu
    Then See "testdir,testfile1.test,testfile6.test,testfile9.test" files are present


  @navigate_with_sidebar
  Scenario: Navigate with sidebar
    * Make "sample.tar.gz" from "testdir,testfile1.test,testfile2.test,testfile3.test"
    * Start file-roller
    * Open "sample.tar.gz" archive
    * Click "Sidebar" in application menu
    * Click the "sample.tar.gz" in sidebar
    * See "testfile1.test,testfile2.test" files are present
    * See "testfile6.test,testfile111.test" files are not present
    * Click the "testdir" in sidebar
    Then See "testfile1.test,testfile2.test" files are not present
    Then See "testfile6.test,testfile111.test" files are present


  @rhbz1442160
  @show_help
  Scenario: Show help
    * Start file-roller
    * Click "Help" in application menu
    Then Verify that help is open


  @about
  Scenario: Show about dialog
    * Start file-roller
    * Click "About" in application menu
    Then Verify that about dialog is open
