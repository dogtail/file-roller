Feature: Nautilus operation


  @rhbz1591792
  @nautilus
  @context_create_tar
  Scenario: Create .tar archive from context menu
    * Start nautilus
    * Navigate to the testing directory in nautilus
    * Using context menu compress "testfile1.test" as ".tar" into testing directory
    * Choose to open the archive from the confirmation dialog
    Then See "testfile1.test" files are present
    Then Check "testfile1.test.tar" contains "testfile1.test"


  @rhbz1591792
  @nautilus
  @context_create_zip
  Scenario: Create .zip archive from context menu
    * Start nautilus
    * Navigate to the testing directory in nautilus
    * Using context menu compress "testfile2.test" as ".zip" into testing directory
    * Choose to open the archive from the confirmation dialog
    Then See "testfile2.test" files are present
    Then Check "testfile2.test.zip" contains "testfile2.test"


  @rhbz1591792
  @nautilus
  @context_create_ar
  Scenario: Create .ar archive from context menu
    * Start nautilus
    * Navigate to the testing directory in nautilus
    * Using context menu compress "testfile2.test" as ".ar" into testing directory
    * Choose to open the archive from the confirmation dialog
    Then See "testfile2.test" files are present
    Then Check "testfile2.test.ar" contains "testfile2.test"


  @rhbz1591792
  @nautilus
  @context_create_jar
  Scenario: Create .jar archive from context menu
    * Start nautilus
    * Navigate to the testing directory in nautilus
    * Using context menu compress "testfile2.test" as ".jar" into testing directory
    * Choose to open the archive from the confirmation dialog
    Then See "testfile2.test" files are present
    Then Check "testfile2.test.jar" contains "testfile2.test"


  @rhbz1591792
  @nautilus
  @context_create_tar_xz
  Scenario: Create .tar.xz archive from context menu
    * Start nautilus
    * Navigate to the testing directory in nautilus
    * Using context menu compress "testfile2.test" as ".tar.xz" into testing directory
    * Choose to open the archive from the confirmation dialog
    Then See "testfile2.test" files are present
    Then Check "testfile2.test.tar.xz" contains "testfile2.test"


  @rhbz1591792
  @nautilus
  @context_create_tar_bz2
  Scenario: Create .tar.bz2 archive from context menu
    * Start nautilus
    * Navigate to the testing directory in nautilus
    * Using context menu compress "testfile2.test" as ".tar.bz2" into testing directory
    * Choose to open the archive from the confirmation dialog
    Then See "testfile2.test" files are present
    Then Check "testfile2.test.tar.bz2" contains "testfile2.test"


  @rhbz1591792
  @nautilus
  @context_create_tar_lzo
  Scenario: Create .tar.lzo archive from context menu
    * Start nautilus
    * Navigate to the testing directory in nautilus
    * Using context menu compress "testfile2.test" as ".tar.lzo" into testing directory
    * Choose to open the archive from the confirmation dialog
    Then See "testfile2.test" files are present
    Then Check "testfile2.test.tar.lzo" contains "testfile2.test"


  @nautilus
  @open_archive_from_files
  Scenario: Open archive from Files
    * Make "sample.zip" from "testdir,testfile1.test,testfile3.test,testfile5.test"
    * Start nautilus
    * Navigate to the testing directory in nautilus
    * Choose to "Open With Archive Manager" the "sample.zip" file using context menu
    Then See "testdir,testfile1.test,testfile3.test,testfile5.test" files are present


  @rhbz1591792
  @nautilus
  @context_create_nonarchive
  Scenario: Create compressed non-archive file from context menu
    * Start nautilus
    * Navigate to the testing directory in nautilus
    * Using context menu compress "testfile3.test" as ".gz" into testing directory
    * Choose to open the archive from the confirmation dialog
    Then See "testfile3.test" files are present
