Feature: Start and stop file-roller in different ways

  @startViaCommand
  Scenario: Start via command
    * Start file-roller via command
    Then file-roller should start

  @startViaMenu
  Scenario: Start via menu
    * Start file-roller via menu
    Then file-roller should start

  @closeViaShortcut
  Scenario: Ctrl-Q to quit application
    * Start file-roller via command
    * Press "<Ctrl><Q>"
    Then file-roller shouldn't be running anymore

  @closeViaGnomePanel
  Scenario: Close via gnome panel
    * Start file-roller via command
    * Click "Quit" in GApplication menu
    Then file-roller shouldn't be running anymore
