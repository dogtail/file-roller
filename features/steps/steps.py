# -*- coding: UTF-8 -*-
from behave import step
from dogtail.tree import root, SearchError
from dogtail.utils import doDelay
from dogtail.rawinput import pressKey, typeText, click, keyCombo, keyNameToKeyCode, registry, release, press as pressbutton
from pyatspi import (KEY_PRESS, KEY_RELEASE)
from common_steps.app import *
from common_steps.appmenu import *
from getpass import getuser
import os.path
import subprocess
import pexpect


def hold_key(key_name):
    code = keyNameToKeyCode(key_name)
    registry.generateKeyboardEvent(code, None, KEY_PRESS)
    doDelay(1)


def release_key(key_name):
    code = keyNameToKeyCode(key_name)
    registry.generateKeyboardEvent(code, None, KEY_RELEASE)
    doDelay(1)

def run(context, command, *a, **kw):
    try:
        output = subprocess.check_output(command, shell=True, stderr=subprocess.STDOUT, *a, **kw).decode('utf8')
        returncode = 0
        exception = None
    except subprocess.CalledProcessError as e:
        output = e.output
        returncode = e.returncode
        exception = e
    context.embed('text/plain', '$?=%d' % returncode, caption='%s result' % command)
    context.embed('text/plain', output, caption='%s output' % command)
    print(("%s output: %s" % (command,output)))
    return output, returncode, exception

def command_output(context, command, *a, **kw):
    output, code, e = run(context, command, *a, **kw)
    if code != 0:
        raise e
    return output

def command_code(context, command, *a, **kw):
    _, code, _ = run(context, command, *a, **kw)
    return code

def wait_for_app_by_name(appname):
    for i in range(0, 10):
        try:
            root.application(appname)
            return
        except (GLib.GError, SearchError):
            sleep(1)
            continue
    raise SearchError("%s did not appear within timeout period")

def clickFocus(frame, maximize=False):
    """ Will focus on the window by clicking in the middle of its frame's titlebar.
    Input a frame or dialog, will try to get its coords and click the titlebar"""
    try:
        coordinates = (frame.position[0]+frame.size[0]/2, frame.position[1]+20)
        if maximize is False:
            click(coordinates[0], coordinates[1])
        else:  # a doubleClick to maximize as well
            doubleClick(coordinates[0], coordinates[1])
    except:
        print("Warning: Could not clickFocus() at the app frame")
        return False


@step('Start file-roller')
def start(context):
    context.app.startViaCommand()
    context.instance = root.application("file-roller")
    # need it there too for some steps from common steps
    context.app.instance = context.instance


@step('Start file-roller with "{params}" parameters')
def start_params(context, params):
    context.terminal_instance = pexpect.spawn("file-roller %s" % params)
    context.instance = root.application("file-roller")
    context.app.instance = context.instance


@step('Start file-roller with "{params}" parameters from the sample archive directory')
def start_params_from_sampledir(context, params):
    os.chdir('/home/%s/Music' % getuser())
    context.terminal_instance = pexpect.spawn("file-roller %s" % params)
    os.chdir(context.curdir)
    context.instance = root.application("file-roller")
    context.app.instance = context.instance


@step('Terminal instance exited with return code "{code}"')
def terminal_return_code(context, code):
    context.terminal_instance.expect(pexpect.EOF)
    context.terminal_instance.close()
    assert context.terminal_instance.signalstatus != 11, "File-roller terminated with segmentation fault"
    assert context.terminal_instance.exitstatus == 0, "Filer-roller ended with exit code: %d" % context.terminal_instance.exitstatus


@step('Start nautilus')
def start_nautilus(context):
    subprocess.Popen('nautilus')
    context.nautilus = root.application("nautilus")
    doDelay(2)
    context.nautilus_frame = context.nautilus.child(name='Home', roleName='frame')
    clickFocus(context.nautilus_frame)


@step('Create new "{typ}" type archive called "{name}" in testing location')
def create_new_archive(context, typ, name):
    context.execute_steps('''
    * Click "New Archive…" in application menu
    * Type "{name}"
    * Select type "{type}"
    * Choose location "Other…"
    * Click "Music" label in the "Location" file chooser
    * Click "Open" push button in the "Location" file chooser
    * Click "Create" push button in the "New Archive" dialog'''.format(name=name, type=typ))


@step('Come into New Archive dialog filling filename "{name}" and type "{typ}"')
def create_new_archive_no_confirm(context, name, typ):
    context.execute_steps('''
    * Click "New Archive…" in application menu
    * Type "%s"
    * Select type "%s"
    * Choose location "Music"''' % (name, typ))


@step('Set password to "{password}" in Other Options section')
def password_new(context, password):
    context.execute_steps('''
    * Click "Other Options" toggle button in the "New Archive" dialog''')
    context.instance.child(roleName="dialog").child("Other Options").doActionNamed('activate')
    context.instance.child(roleName="dialog").child("Password:").labelee.click()
    typeText(password)


@step('Add "{files}" files into open archive')
def add_files_in_archive(context, files):
    context.execute_steps('''
    * Click "Add" push button in the panel
    * Click "Music" label in the "Add Files" dialog
    * Check-in files "%s"
    * Click "Add" push button in the "Add Files" dialog''' % (files))


@step('Open "{archive}" archive')
def open_existing_archive(context, archive):
    context.execute_steps('''
    * Click "Open…" in application menu
    * Double-click "Music" table cell in the "Open" file chooser
    * Double-click "%s" table cell in the "Open" file chooser
    #* Click "Open" push button in the "Open" file chooser''' % (archive))


@step('Using context menu compress "{file}" as "{type}" into testing directory')
def compress_archive_context_menu(context, file, type):
    context.execute_steps('''
    * Choose to "Compress..." the "%s" file using context menu
    * Select type "%s"
    * Choose location "Music"
    * Click "Create" push button in the "Compress" dialog''' % (file, type))


@step('Spawn context menu and delete "{option}"')
def delete_general(context, option):
    if '*' not in option:
        context.execute_steps('''
        * Invoke context menu over "testfile1.test" and choose "Delete" action
        * Click "%s" radio button in the dialog
        * Click "Delete" to confirm the dialog''' % (option))
    else:
        context.execute_steps('''
        * Invoke context menu over "testfile1.test" and choose "Delete" action
        * Click "Files:" radio button in the dialog''')
        context.instance.child(roleName='dialog').child(roleName='text').click()
        context.execute_steps('''
        * Type "%s"
        * Click "Delete" to confirm the dialog''' % (option))


@step('Extract files into the testing directory')
def extract_in_testingdir(context):
    context.execute_steps('''
    * Click "Music" label in the "Extract" file chooser
    * Click the object with "Create Folder" description
    * Type "extracted"
    * Wait for "0.5" seconds
    * Press key "Enter"
    * Wait for "0.5" seconds
    * Click "Extract" push button in the "Extract" file chooser''')


@step('Extract files into the current directory')
def extract_in_currentdir(context):
    context.execute_steps('''
    * Click "Extract" push button in the "Extract" file chooser''')


@step('Type "{text}" and save file in gedit')
def gedit_edit(context, text):
    # may take a while
    wait_for_app_by_name('gedit')
    doDelay(1) # not to put first characters elsewhere
    context.execute_steps('''
    * Type "%s"
    * Invoke "<ctrl>s" shortcut
    * Invoke "<ctrl>q" shortcut''' % text)


@step('Set file filter to "{filt}" and extract into the testing directory')
def extract_in_testingdir_filter(context, filt):
    context.execute_steps('''
    * Click the object with "Create Folder" description
    * Type "extracted"
    * Wait for "0.5" seconds
    * Press key "Enter"
    * Wait for "0.5" seconds
    * Click "Files:" radio button in the "Extract" file chooser
    * Press key "\t"
    * Type "%s"
    * Wait for "0.5" seconds
    * Click "Extract" push button in the "Extract" file chooser''' % filt)


@step('Click "{name}" {roleName} in the {par_roleName}')
@step('Click "{name}" {roleName} in the "{parent}" {par_roleName}')
def click_anything(context, name, roleName, par_roleName, parent=None):
    context.instance.child(name=parent, roleName=par_roleName).child(name, roleName=roleName).click()


@step('Double-click "{name}" {roleName} in the {par_roleName}')
@step('Double-click "{name}" {roleName} in the "{parent}" {par_roleName}')
def doubleclick_anything(context, name, roleName, par_roleName, parent=None):
    context.instance.child(name=parent, roleName=par_roleName).child(name, roleName=roleName).doubleClick()


@step('Add text "{text}" in extracted file "{file}"')
def add_text_in_file(context, text, file):
    command_code(context, "echo '%s' >> /home/%s/Music/extracted/%s" % (text, getuser(), file))


@step('Check extracted file "{file}" contains "{text}" text')
def check_text_in_file(context, file, text):
    doDelay(1)
    output = command_output(context, "cat /home/%s/Music/extracted/%s" % (getuser(), file))
    assert text in output, "The text has not been found within file"


@step('Check archive "{archive}" requires a "{password}" password and contains "{files}"')
def check_password(context, archive, password, files):
    doDelay(2)
    atool = pexpect.spawn("atool -X /home/%s/Music/extracted/ /home/%s/Music/%s" % (getuser(), getuser(), archive))
    atool.expect("password")
    atool.sendline(password)
    context.execute_steps('''* Check files "%s" have been extracted''' % files)


@step('Toggle {state} the "{name}" check box')
def toggle_check_box(context, state, name):
    toggle = context.instance.child(name=name, roleName='check box')
    if state == 'on':
        if toggle.checked is False:
            toggle.click()
    else:
        if toggle.checked is True:
            toggle.click()


@step('Choose and open with "{name}" application')
def choose_app_open(context, name):
    dialog = context.instance.child(name="Select Application", roleName='dialog')
    dialog.child("View All Applications", roleName="push button").click()
    table_app = dialog.child(name, roleName='table cell')
    table_app.grabFocus()
    dialog.child("Select", roleName='push button').click()


@step('Look for text "{text}" in the report')
def look_for_text_report(context, text):
    dialog = context.instance.child(name="Test Result", roleName='dialog')
    area = dialog.child(roleName="text")
    assert text in area.text


@step('Check the "{archive}" properties information is valid')
def check_properties(context, archive):
    dialog = context.instance.child(name="%s Properties" % archive, roleName='dialog')
    if archive.split('.')[-1] == 'zip':
        dialog.child("application/zip")
    elif archive.split('.')[-1] == 'cpio':
        dialog.child("application/x-cpio")
    elif archive.split('.')[-1] == 'gz':
        dialog.child("application/x-compressed-tar")
    dialog.child("10")
    dialog.child("/home/%s/Music" % getuser())

@step('Choose to open the archive from the confirmation dialog')
def click_open_archive_nautilus(context):
    context.instance.child(roleName="alert").child("Open the Archive", roleName="push button").click()
    doDelay(3)


@step('Click the toolbar "{button}" button')
def click_toolbar_button(context, button):
    toolbar = context.instance.child(roleName="panel")
    if button == "Search":
        toolbar.child(description="Find files by name").click()
    elif button == "Home":
        context.instance.child(roleName="push button", description="Go to the home location").click()
    else:
        toolbar.child(button).click()


@step('Click the object with "{description}" description')
def click_sidebar_item(context, description):
    context.instance.child(description=description).click()


@step('Click the "{item}" in sidebar')
def click_sidebar_item(context, item):
    sidebar = context.instance.child(roleName="tree table")
    sidebar.child(item).click()


@step('Choose "{item}" from the archive menu')
def choose_menu_item(context, item):
    toolbar = context.instance.child(roleName="panel")
    toolbar.child("Menu").click()
    context.instance.child(item, roleName="push button").click()


@step('Choose location "{typ}"')
@step('Select type "{typ}"')
def choose_filetype(context, typ):
    context.instance = root.application("file-roller")
    context.instance.child(name=typ, roleName='menu item').doActionNamed('click')


def print_icon_labeler_names():
    gs = root.application('gnome-shell')
    for node in gs[0][0][3].findChildren(lambda x: x.labeler.roleName == "label", isLambda=True):
        print((node.labeler.name))


@step('Click gnome-shell topbar "{icon}" icon')
def click_gs_icon(context, icon):
    gs = root.application('gnome-shell')
    gs[0][0][3].findChildren(lambda x: x.labeler.name == icon and x.labeler.roleName == "label", isLambda=True)[0].click()


@step('Check "{archive}" contains "{files}"')
def check_archive_contains(context, archive, files, n=None):
    doDelay(1)
    names = files.split(',')
    if ".iso" in archive:
        output = command_output(context, 'iso-info -f -i /home/%s/Music/%s' % (getuser(), archive))
    else:
        output = command_output(context, 'atool -l /home/%s/Music/%s' % (getuser(), archive))
    if n is None:
        for name in names:
            if name not in output:
                raise Exception('Archive /home/%s/Music/%s does not contains %s' % (getuser(), archive, name))
    else:
        for name in names:
            if name in output:
                raise Exception('Archive /home/%s/Music/%s contains %s and it should not' % (getuser(), archive, name))


@step('Check "{archive}" does not contain "{files}"')
def check_archive_not_contains(context, archive, files):
    check_archive_contains(context, archive, files, n=True)


@step('Check "{archive}" archive is empty')
def check_archive_empty(context, archive):
    try:
        output = command_output(context, 'atool -l /home/%s/Music/%s' % (getuser(), archive))
    except:
        return # some archivers return nonzero code when empty - that is okay too
    assert "test" not in output , "Archive is not empty, contains:\n%s" % output


@step('Check files "{files}" have {n} been extracted')
@step('Check files "{files}" have been extracted')
def check_extracted(context, files, n=None):
    names = files.split(',')
    output = subprocess.check_output('find /home/%s/Music/extracted' % getuser(), shell=True).decode('utf8')
    for name in names:
        if n is None:
            if name not in output:
                raise Exception('"%s" not found in extracted directory' % name)
        else:
            if name in output:
                raise Exception('"%s" found in extracted directory' % name)


@step('File "{path}" exists')
def check_file(context, path):
    assert os.path.isfile(path), "File %s does not exist" % path


@step('Make "{archive}" from "{files}"')
def make_archive(context, archive, files):
    names = files.split(',')
    os.chdir('/home/%s/Music' % getuser())
    command_code(context, 'atool -a %s %s' % (archive, ' '.join(names)))
    os.chdir(context.curdir)
    if len(archive.split('.')) == 2 and archive.split('.')[-1] == 'gz':
        return
    context.execute_steps('* Check "%s" contains "%s"' % (archive, files))


@step('Make encrypted zip "{archive}" containing "{files}" files')
def make_encr_archive(context, archive, files):
    names = files.split(',')
    os.chdir('/home/%s/Music' % getuser())
    command_code(context, 'zip --password redhat %s %s' % (archive, ' '.join(names)))
    os.chdir(context.curdir)
    context.execute_steps('* Check "%s" contains "%s"' % (archive, files))


@step('See "{files}" files are present')
@step('See "{files}" files are {n} present')
def see_files(context, files, n=None):
    context.instance = root.application("file-roller")
    names = files.split(',')
    if n is None:
        for name in names:
            context.instance.child(name=name, roleName='table cell').point()
    else:
        for name in names:
            try:
                context.instance.child(name=name, roleName='table cell', retry=False)
                assert False, "The file %s is present and it should not be!" % (name)
            except SearchError:
                pass


@step('Select files "{files}"')
def select_files(context, files):
    names = files.split(',')
    hold_key('Control_L')
    for name in names:
        doDelay(0.5)
        x, y = context.instance.child(name=name, roleName='table cell').position
        pressbutton(x + 10, y + 10)
        doDelay(0.5)
        release(x + 10, y + 10)
        doDelay(0.5)
    release_key('Control_L')


@step('Check-in file "{fil}"')
def checkin_file(context, fil):
    parent = context.instance.child(roleName='dialog').child(fil, roleName='table cell').parent
    parent[0].click()


@step('Check-in files "{files}"')
def checkin_files(context, files):
    for fil in files.split(','):
        parent = context.instance.child(roleName='dialog').child(fil, roleName='table cell').parent
        parent[0].click()
        doDelay(0.4)


@step('Run "{command}" in background')
def runcommand(context, command):
    subprocess.Popen(command + " > /dev/null", shell=True)


@step('Type "{text}"')
def type(context, text):
    typeText(text)


@step('Invoke "{shortcut}" shortcut')
def shortcut_keycombo(context, shortcut):
    keyCombo(shortcut)


@step('Type "{text}" and confirm')
def type_confirm(context, text):
    typeText(text)
    doDelay(0.5)
    pressKey('Enter')
    doDelay(2)


@step('Press key "{key}"')
def press(context, key):
    pressKey(key)


@step('Wait for "{x}" seconds')
def waitfor(context, x):
    doDelay(float(x))


@step('Choose to "{what}" the "{fil}" file using context menu')
def file_action(context, what, fil):
    context.nautilus.child(name=fil).click(3)
    context.nautilus.child(what, roleName='menu item').doActionNamed('click')


@step('Set "{filter}" filter to "{text}" text')
def set_filter(context, filter, text):
    dialog = context.instance.child(roleName="dialog", recursive=False)
    textarea = dialog.child(filter+":").labelee
    textarea.text = ""
    doDelay(0.5)
    textarea.typeText(text)


@step('Invoke context menu over "{filename}" and choose "{action}" action')
def current_mouse_contextmenu_action(context, filename, action):
    context.instance.child(filename).click(3)
    context.instance.child(action, roleName='menu item').click()


@step('Navigate to the testing directory in the dialog')
def navigate_to_music(context):
    context.execute_steps('''* Click "Music" label in the "Add Files" dialog''')


@step('Click "{button}" to confirm the dialog')
def click_button_in_dialog(context, button):
    dialog = context.instance.findChild(lambda x: x.roleName == 'dialog' or x.roleName == 'alert' or x.roleName == 'file chooser')
    dialog.child(button, roleName='push button').click()


@step('Navigate to the testing directory in nautilus')
@step('Go to the "{folder}" folder in nautilus')
def click_nautilus(context, folder="Music"):
    typeText(folder)
    #doDelay(2)
    #context.nautilus_frame.child(name='All Files', roleName='radio button').doActionNamed('click')
    doDelay(5)
    pressKey('enter')


@step('Verify that help is open')
def help_open(context):
    yelp = root.application("yelp")
    frame = yelp.child("Archive Manager Help", roleName="frame")
    frame.child("Archive Manager", roleName="heading")


@step('Verify that about dialog is open')
def about_open(context):
    about = context.instance.child("About Archive Manager", roleName="dialog")
    about.child("An archive manager for GNOME.")
