#!/bin/env python

# Connect to TCMS and grab test cases in a feature file
# Parameter: get_feature_from_tcms.py <tcms test plan ID> <feature file>

import codecs
from HTMLParser import HTMLParser
from optparse import OptionParser


usage = "usage: %prog options"
parser = OptionParser(usage=usage)
parser.add_option("-i", "--id", dest="tcms_plan_id",
                  help="read steps from tcms plan id", metavar="ID")
parser.add_option("-f", "--feature", dest="feature_file_name",
                  help="write feature to FILE", metavar="FILE")
(options, args) = parser.parse_args()
if ((options.tcms_plan_id is None) or (options.feature_file_name is None)):
    parser.error("incorrect number of arguments")

try:
    from nitrate import *
except ImportError:
    raise RuntimeError("Install python-nitrate from http://psss.fedorapeople.org/python-nitrate/")

class Parser(HTMLParser):

    def handle_starttag(self, tag, attrs):
        if tag not in self.tag_started.keys():
            self.tag_started[tag] = 0

        self.tag_started[tag] += 1

        if tag == 'table':
            self.result[-1]['table'] = []

        if tag == 'tr':
            self.result[-1]['table'].append([])

        if tag == 'ul':
            self.action_counter += 1

    def handle_endtag(self, tag):
        self.tag_started[tag] -= 1

    def handle_data(self, data):
        if 'li' in self.tag_started.keys() and self.tag_started['li'] > 0:

            # Store pre data in a special """ string
            if 'pre' in self.tag_started.keys() and self.tag_started['pre']:
                self.result[-1]['text'] += '\n"""\n%s\n"""' % data.strip()
                return

            # Store cell data as a list
            if 'td' in self.tag_started.keys() and self.tag_started['td']:
                self.result[-1]['table'][-1].append(data.strip())
                return

            # Skip data in trs
            if 'tr' in self.tag_started.keys() and self.tag_started['tr']:
                return

            # Skip data in tables
            if 'table' in self.tag_started.keys() and self.tag_started['table']:
                return

            # Which action does this result belong to
            # If action has several results - set their action_ids to one action
            if self.tag_started['li'] == 1:
                self.action_counter += 1

            # just a usual action
            self.result.append({})
            self.result[-1]['text'] = data.strip()
            self.result[-1]['action_id'] = self.action_counter

    def to_gherkin_steps(self, html_string):
        self.result = []
        self.tag_started = {}
        self.action_counter = 0
        self.sequential_result = False
        self.feed(html_string)
        return self.result

with codecs.open(options.feature_file_name, 'w', 'utf-8') as feature_file:

    background_written = False

    # Get the testplan object
    test_plan = TestPlan(options.tcms_plan_id)

    # Read test plan name
    feature_file.write("@testplan_%s\n" % test_plan.id)
    feature_file.write("Feature: %s\n\n" % test_plan.name)

    # Reading gherkin scenario
    for test_case in sorted(test_plan.testcases):
        test_data = Nitrate._connection.TestCase.get_text(test_case.id)

        # Parsing actions
        feature_background = Parser().to_gherkin_steps(test_data['setup'])
        scenario_actions = Parser().to_gherkin_steps(test_data['action'])
        scenario_effects = Parser().to_gherkin_steps("<li>" + test_data['effect'] + "</li>")

        # Write setup to feature's background once
        # Note, that this requries EACH step to have the same setup
        if not background_written:
            if len(feature_background) > 0:
                feature_file.write("  Background:\n")
                for index, background in enumerate(feature_background):
                    feature_file.write("    * %s\n" % background['text'])
                feature_file.write("\n")
            background_written = True

        feature_file.write("  @testcase_%s\n" % test_case.id)
        feature_file.write("  Scenario: %s\n" % test_case.summary)

        def write_step(prefix, step_data):
            feature_file.write("    %s %s\n" % (prefix, step_data['text'].strip()))
            if 'table' in step_data.keys() and step_data['table']:
                # Handle tables tag in actions
                cell_count = None
                for index, row in enumerate(step_data['table']):
                    # Remember the number of cell in heading
                    # as some rows might not have enough cells
                    if index == 0:
                        cell_count = len(row)
                    feature_file.write("      |")
                    for cell in row:
                        feature_file.write(" %s |" % cell.strip())
                    # Add a required number of empty cells
                    for i in range(0, cell_count - len(row)):
                        feature_file.write(" |")
                    feature_file.write("\n")
                feature_file.write("\n")

        # Write steps and expected results in the scenario
        for action in scenario_actions:
            write_step("*", action)

        # Write last effect
        write_step("Then", scenario_effects[-1])
        feature_file.write("\n")
