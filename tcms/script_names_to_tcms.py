#!/usr/bin/python
# -*- coding: utf-8 -*-

import argparse
import logging
import nitrate
import subprocess
import sys
from behave import parser as gherkin_parser

descr = '''script_names_to_tcms.py - match automated (set to auto) cases from a tcms plan with a behave
feature file's scenarios and use the tags as testname=xxx in the script field. Matches are based on the
test case summary and scenario names. Only requirement on tags: the last tag before Scenario: counts. Script will
let you review any changes before doing the update. Does not touch cases with some script already set unless
--force-script is set.

You can run this against a single plan several times for different features.'''

logging.basicConfig(format='%(message)s')

log = logging.getLogger()
log.setLevel(logging.INFO)

parser = argparse.ArgumentParser(prog='$ script_names_to_tcms.py', description=descr, formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('--plan', required=True, help="""Numeric ID of tcms plan in question""")
parser.add_argument('--feature', required=True, help="""Path to a feature file to match against""")
parser.add_argument('--force-script', action='store_true', help="""Update script fields of matching cases even when already set""")

args = parser.parse_args()

plan = nitrate.TestPlan(int(args.plan))
feature = gherkin_parser.parse_file(args.feature)

testcases_to_updated = []
for testcase in plan.testcases:
    log.info("Working on: %s" % testcase)
    for scenario in feature.scenarios:
        if testcase.automated and testcase.summary.strip() == scenario.name.strip():
            log.info("Found match with:")
            log.info("Tags: " + ' '.join(scenario.tags))
            log.info("Name: " + scenario.name)
            if testcase.script is None or args.force_script:
                testcase.script = u"testname=%s" % scenario.tags[-1]
                testcases_to_updated.append(testcase)
            else:
                log.warning("Not updating as script is already set and --force_script not used.")
    print ("\n")

if len(testcases_to_updated) == 0:
    log.info("Did not find any matches. Exiting...")
    sys.exit(1)

log.info("Done matching testcases, ready to update these [%d] cases:" % len(testcases_to_updated))
for testcase in testcases_to_updated:
    print "%s: %s" % (testcase.summary, testcase.script)

yesno = raw_input("\nShall I uptate the test cases? (yes/no): ")
if yesno == "yes":
    for testcase in testcases_to_updated:
        log.info("Updating %s" % testcase)
        testcase.update()
    log.info("Testcases updated, exiting...")
else:
    log.info("Not proceeding, exiting...")
    sys.exit(1)
